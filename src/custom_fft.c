#include "custom_fft.h"
#include <math.h>
#include <stdlib.h>



Complex add_complex(Complex A, Complex B)
{
    Complex result;
    result.re = A.re + B.re;
    result.im = A.im + B.im;
    return result;
}


Complex multiply_complex(Complex A, Complex B)
{
    Complex result;
    result.re = (A.re * B.re) - (A.im * B.im);
    result.im = (A.re * B.im) + (A.im * B.re);
    return result;
}


Complex multiply_scalar(Complex A, float B)
{
    Complex result;
    result.re = A.re * B;
    result.im = A.im * B;
    return result;
}


void separate_data(const unsigned num_samples, const Complex *signal,
        Complex *even_data, Complex *odd_data)
{
    const unsigned Ns = num_samples;
    for( unsigned i = 0; i < Ns/2; ++i ){
        unsigned i1 = 2*i;
        unsigned i2 = 2*i + 1;
        even_data[i] = signal[i1];
        odd_data[i] = signal[i2];
    }
}


// DFT with k constant
Complex dft_k( const unsigned Ns, const Complex *signal, const unsigned k )
{
    Complex result;
    result.re = 0;
    result.im = 0;
    for( unsigned n = 0; n < Ns; ++n ){
        result.re += signal[n].re * cos(2*M_PI*k*n/Ns) + signal[n].im * sin(2*M_PI*k*n/Ns);
        result.im += -signal[n].re * sin(2*M_PI*k*n/Ns) + signal[n].im * cos(2*M_PI*k*n/Ns);
    }

    return result;
}


Complex cooley_tukey(const unsigned num_samples, const Complex *signal, const unsigned k)
{
    Complex result;
    const unsigned Ns = num_samples;

    // Recurrency threshold
    unsigned threshold = 16;
    // Check if Ns is not even
    if(( Ns % 2 != 0 )||(Ns < threshold)){
        // Compute DFT
        result = dft_k( Ns, signal, k );
    }else{
        // twiddle_factor
        Complex tf;
        tf.re = cos( 2.*M_PI*k/Ns );
        tf.im = -sin( 2.*M_PI*k/Ns );

        Complex *even_data = malloc( (Ns/2) * sizeof(Complex) );
        Complex *odd_data = malloc( (Ns/2) * sizeof(Complex) );
        separate_data(Ns, signal, even_data, odd_data);

        Complex even = cooley_tukey( Ns/2, even_data, k );
        Complex odd = cooley_tukey( Ns/2, odd_data, k );

        result = add_complex( even, multiply_complex(tf, odd) );

        free( even_data );
        free( odd_data );
    }

    return result;
}



void fft(const unsigned num_samples, const Complex *signal, Complex *transform )
{
    // In cooley_tukey() the number of samples must be even,
    // otherwhise we would compute the DFT directly
    if( num_samples % 2 != 0 )
        exit( EXIT_FAILURE );

    const unsigned Ns = num_samples;

    for( unsigned k = 0; k <= Ns/2; ++k){
        transform[k] = cooley_tukey( Ns, signal, k );
    }// for k

    // Copy lower half values from transform
    for( unsigned k = 1; k < Ns/2; ++k){
        unsigned upper_k = Ns - k;
        transform[upper_k].re = transform[k].re;
        transform[upper_k].im = -transform[k].im;
    }
}




Complex idft_n(const unsigned Ns, const Complex *transform, const unsigned n)
{
    Complex result;
    result.re = 0;
    result.im = 0;
    for( unsigned k = 0; k < Ns; ++k ){
        result.re += transform[k].re * cos(2*M_PI*k*n/Ns) - transform[k].im * sin(2*M_PI*k*n/Ns);
        result.im += transform[k].re * sin(2*M_PI*k*n/Ns) + transform[k].im * cos(2*M_PI*k*n/Ns);
    }

    return result;
}


Complex icooley_tukey(const unsigned num_samples, const Complex *transform,
        const unsigned n)
{
    Complex result;
    const unsigned Ns = num_samples;

    // Recurrency threshold
    unsigned threshold = 16;
    // Check if Ns is not even
    if(( Ns % 2 != 0 )||(Ns < threshold)){
        // Compute DFT
        result = idft_n( Ns, transform, n );
    }else{
        // twiddle_factor
        Complex tf;
        tf.re = cos( 2.*M_PI*n/Ns );
        tf.im = sin( 2.*M_PI*n/Ns );

        Complex *even_data = malloc( (Ns/2) * sizeof(Complex) );
        Complex *odd_data = malloc( (Ns/2) * sizeof(Complex) );
        separate_data(Ns, transform, even_data, odd_data);

        Complex even = icooley_tukey( Ns/2, even_data, n );
        Complex odd = icooley_tukey( Ns/2, odd_data, n );

        result = add_complex( even, multiply_complex(tf, odd) );

        free( even_data );
        free( odd_data );
    }

    return result;
}


void ifft(const unsigned num_samples, const Complex *transform, Complex *signal )
{
    // In cooley_tukey() the number of samples must be even,
    // otherwhise we would compute the DFT directly
    if( num_samples % 2 != 0 )
        exit( EXIT_FAILURE );

    const unsigned Ns = num_samples;

    for( unsigned n = 0; n <= Ns/2; ++n){
        Complex x = icooley_tukey( Ns, transform, n );
        signal[n] = multiply_scalar( x, 1./Ns );
    }// for n

    // Copy lower half values from transform
    for( unsigned n = 1; n < Ns/2; ++n){
        unsigned upper_n = Ns - n;
        signal[upper_n].re = -signal[n].re;
        signal[upper_n].im = signal[n].im;
    }
}


