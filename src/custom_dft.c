#include "custom_dft.h"
#include <math.h>


void dft( const unsigned num_samples, const Complex *signal, Complex *transform )
{
    const unsigned Ns = num_samples;

    for( unsigned k = 0; k < Ns; ++k){
        float Xr = 0;
        float Xi = 0;
        for( unsigned n = 0; n < Ns; ++n ){
            Xr += signal[n].re * cos(2*M_PI*k*n/Ns) + signal[n].im * sin(2*M_PI*k*n/Ns);
            Xi += -signal[n].re * sin(2*M_PI*k*n/Ns) + signal[n].im * cos(2*M_PI*k*n/Ns);
        }
        transform[k].re = Xr;
        transform[k].im = Xi;
    }
}


void idft( const unsigned num_samples, const Complex *transform, Complex *signal )
{
    const unsigned Ns = num_samples;

    for( unsigned n = 0; n < Ns; ++n){
        float xr = 0;
        float xi = 0;
        for( unsigned k = 0; k < Ns; ++k ){
            xr += transform[k].re * cos(2*M_PI*k*n/Ns) - transform[k].im * sin(2*M_PI*k*n/Ns);
            xi += transform[k].re * sin(2*M_PI*k*n/Ns) + transform[k].im * cos(2*M_PI*k*n/Ns);
        }
        signal[n].re = xr/Ns;
        signal[n].im = xi/Ns;
    }
}


void dft_even( const unsigned num_samples, const Complex *signal, Complex *transform )
{
    const unsigned Ns = num_samples;

    for( unsigned k = 0; k <= Ns/2; ++k){
        float Xr = 0;
        float Xi = 0;
        for( unsigned n = 0; n < Ns; ++n ){
            Xr += signal[n].re * cos(2*M_PI*k*n/Ns) + signal[n].im * sin(2*M_PI*k*n/Ns);
            Xi += -signal[n].re * sin(2*M_PI*k*n/Ns) + signal[n].im * cos(2*M_PI*k*n/Ns);
        }
        transform[k].re = Xr;
        transform[k].im = Xi;
    }

    // Copy lower half values from transform
    // Reduces computation cost but requires Ns to be even !!!
    // https://dsp.stackexchange.com/questions/8715/symmetry-of-real-and-imaginary-parts-in-fft
    // X(N-n) = *X(n) 
    for( unsigned k = 1; k < Ns/2; ++k){
        unsigned upper_k = Ns - k;
        transform[upper_k].re = transform[k].re;
        transform[upper_k].im = -transform[k].im;
    }
}


void idft_even( const unsigned num_samples, const Complex *transform, Complex *signal )
{
    const unsigned Ns = num_samples;

    for( unsigned n = 0; n < Ns/2; ++n){
        float xr = 0;
        float xi = 0;
        for( unsigned k = 0; k < Ns; ++k ){
            xr += transform[k].re * cos(2*M_PI*k*n/Ns) - transform[k].im * sin(2*M_PI*k*n/Ns);
            xi += transform[k].re * sin(2*M_PI*k*n/Ns) + transform[k].im * cos(2*M_PI*k*n/Ns);
        }
        signal[n].re = xr/Ns;
        signal[n].im = xi/Ns;
    }

    for( unsigned n = 1; n < Ns/2; ++n){
        unsigned upper_n = Ns - n;
        signal[upper_n].re = -signal[n].re;
        signal[upper_n].im = signal[n].im;
    }
}


