// Thomio Watanabe
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "custom_dft.h"



typedef struct{
    // Sampling frequency ( >= 2*fmax )
    float Fs;
    // Sampling period
    float T;
    // Time interval
    float I;
    // Number of samples
    unsigned Ns;
} DFTParams;


float degree_to_radians( float degree )
{
    return degree * M_PI / 180;
}


float radians_to_degree( float radians )
{
    return radians * 180 / M_PI;
}


// Generate signal composed of 3 sin waves
void generate_wave( DFTParams dft_params, Complex *wave )
{
    // Sampling frequency ( >= 2*fmax )
    float fs = dft_params.Fs;
    // Sampling period
    float T = dft_params.T;
    // Time interval
    float I = dft_params.I;
    // Number of samples
    unsigned Ns = dft_params.Ns;

    float a1, a2, a3, f1, f2, f3;
    a1 = a2 = a3 = 1;
    // Frequency in hertz
    f1 = 30;
    f2 = 45;
    f3 = 90;

    printf("\nGenerating sampled wave:\n");
    printf("Sampling frequency = %.1fHz\n", fs);
    printf("Time interval = %fs\n", I);
    printf("Number of samples = %d\n", Ns);
    printf("Frequencies(Hz) = %2.f, %2.f, %2.f\n\n", f1,f2,f3);

    for(size_t i = 0; i < Ns; ++i){
        // t is time in seconds
        float t = i * T;
        float s1 = a1 * sin( 2* M_PI * f1 * t );
        float s2 = a2 * sin( 2* M_PI * f2 * t );
        float s3 = a3 * sin( 2* M_PI * f3 * t );

        wave[i].re = s1 + s2 + s3;
        wave[i].im = 0;
    }
}


// https://www.mathworks.com/help/matlab/ref/fft.html?s_tid=gn_loc_drop
void extract_freqs( DFTParams dft_params, Complex *transform )
{
    unsigned Ns = dft_params.Ns;
    unsigned Fs = dft_params.Fs;

    for( size_t k = 0; k < Ns/2; ++k ){
        float re = transform[k].re;
        float im = transform[k].im;
        float abs = sqrt( pow(re,2) + pow(im,2) ) / Ns;
        float angle = atan2(im, re);
        float freq = k * Fs/Ns;
        printf("(Mag, freq) = (%.4f, %2.f Hz) \n", abs, freq);
    }
}


void compare_signals( DFTParams dft_params, Complex *original_signal,
        Complex *recovered_signal )
{
    unsigned Ns = dft_params.Ns;
    float acc_error = 0;

    for( unsigned i = 0; i < Ns; ++i ){
        float re = original_signal[i].re - recovered_signal[i].re;
        float im = original_signal[i].im - recovered_signal[i].im;
        acc_error += sqrt( pow(re,2) + pow(im,2) );
    }

    printf("\nAccumulated signal error = %f\n", acc_error );
}



int main()
{
    printf("Custom DFT library example.\n");

    DFTParams dft_params;
    dft_params.Fs = 4*90;
    dft_params.I = 1./30;
    dft_params.T = 1./dft_params.Fs;
    dft_params.Ns = 2.*dft_params.I / dft_params.T;

    Complex *signal = malloc( dft_params.Ns * sizeof(Complex) );
    Complex *transform = malloc( dft_params.Ns * sizeof(Complex) );
    Complex *recovered_signal = malloc( dft_params.Ns * sizeof(Complex) );

    generate_wave( dft_params, signal );

    dft( dft_params.Ns, signal, transform );

    extract_freqs( dft_params, transform );

    idft( dft_params.Ns, transform, recovered_signal );

    compare_signals( dft_params, signal, recovered_signal );


    free(signal);
    free(transform);
    free(recovered_signal);

    return 0;
}

