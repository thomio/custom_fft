#ifndef CUSTOM_FFT_H_
#define CUSTOM_FFT_H_
// Thomio Watanabe
// December 2017

// Custom FFT library
// Requirements:
// - Each function should also print the CPU time and the CPU cycles used.
// - Run in single core

//  There are several algorithms that implement FFT
//  The FFT is a DFT with smaller computational cost. From O(n2) to O(nlog n).
//  https://en.wikipedia.org/wiki/Fast_Fourier_transform
//  https://en.wikipedia.org/wiki/Discrete_Fourier_transform

//  Cooley–Tukey algorithm
//  https://en.wikipedia.org/wiki/Cooley–Tukey_FFT_algorithm

// "The algoritm re-expresses the discrete Fourier transform (DFT) of an 
//  arbitrary composite size N = N1N2 in terms of N1 smaller DFTs of sizes N2,
//  recursively, to reduce the computation time to O(N log N) for highly
//  composite N"

//  Divides the transform into two pieces of size N/2 at each step, and is
//  therefore limited to power-of-two sizes



typedef struct {
   float re;
   float im;
} Complex;


Complex add_complex( Complex A, Complex B );
Complex multiply_complex( Complex A, Complex B );
Complex multiply_scalar( Complex A, float B );


void separate_data(const unsigned num_samples, const Complex *signal,
        Complex *even_data, Complex *odd_data);


Complex cooley_tukey(const unsigned num_samples, const Complex *signal,
        const unsigned k);

Complex dft_k( const unsigned Ns, const Complex *signal, const unsigned k);

// Perform out = FFT(in)
void fft(const unsigned num_samples, const Complex *signal, Complex *transform );


Complex icooley_tukey(const unsigned num_samples, const Complex *transform,
        const unsigned n);

Complex idft_n( const unsigned Ns, const Complex *transform, const unsigned n);

// Perform out = inverse FFT(in)
void ifft(const unsigned num_samples, const Complex *transform, Complex *signal );


#endif
