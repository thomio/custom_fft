#ifndef CUSTOM_DFT_H_
#define CUSTOM_DFT_H_
// Thomio Watanabe
// December 2017

//  There are several algorithms that implement FFT
//  The FFT is a DFT with smaller computational cost. From O(n2) to O(nlog n).
//  https://en.wikipedia.org/wiki/Fast_Fourier_transform
//  https://en.wikipedia.org/wiki/Discrete_Fourier_transform


typedef struct {
    float re;
    float im;
} Complex;


void dft( const unsigned num_samples, const Complex *signal, Complex *transform );
void idft( const unsigned num_samples, const Complex *transform, Complex *signal );

void dft_even( const unsigned num_samples, const Complex *signal, Complex *transform );
void idft_even( const unsigned num_samples, const Complex *transform, Complex *signal );

#endif
